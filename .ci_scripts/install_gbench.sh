#!/bin/bash
set -e
mkdir build_system_gbench
pushd build_system_gbench
git clone https://github.com/google/benchmark.git gbench
mkdir gbench_build
pushd gbench_build
cmake -DBENCHMARK_DOWNLOAD_DEPENDENCIES=ON\
      -DCMAKE_CXX_STANDARD=17\
      -DBENCHMARK_ENABLE_TESTING=OFF\
      -DCMAKE_BUILD_TYPE=Release\
      ../gbench
cmake --build . -- install
popd
popd
