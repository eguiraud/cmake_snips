#!/bin/bash
set -e
mkdir build
pushd build
cmake ..
cmake --build .
./gbench/bench
