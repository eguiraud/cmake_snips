# Download google benchmark at CMake configuration time, add it as a subproject
function(FetchGBench)
   include(FetchContent)
   FetchContent_Declare(
      benchmark
      GIT_REPOSITORY https://github.com/google/benchmark
      GIT_TAG master
   )
   FetchContent_Populate(benchmark)
   set(BENCHMARK_DOWNLOAD_DEPENDENCIES ON CACHE BOOL "Download gtest if needed")
   set(BENCHMARK_ENABLE_TESTING OFF CACHE BOOL "Disable building of gbench tests (C++17-incompatible)")
   set(BENCHMARK_ENABLE_INSTALL OFF CACHE BOOL "Disable installation of gbench (it's a private dependency)")
   add_subdirectory(${benchmark_SOURCE_DIR} ${benchmark_BINARY_DIR})
   add_library(benchmark::benchmark ALIAS benchmark)
endfunction()
