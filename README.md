# CMake snips [![pipeline status](https://gitlab.com/bluehood/cmake_snips/badges/master/pipeline.svg)](https://gitlab.com/bluehood/cmake_snips/pipelines)

A curated collection of cmake scripts and modules that just work, for C++ projects.

### Modules
- [FindGBench](https://gitlab.com/bluehood/cmake_snips/blob/master/cmake/FetchGBench.cmake) - download, configure and build [Google benchmark](https://github.com/google/benchmark) using cmake's [FetchContent](https://cmake.org/cmake/help/latest/module/FetchContent.html)

### Templates:
- [gbench](https://gitlab.com/bluehood/cmake_snips/tree/master/gbench) - example project depending on [Google benchmark](https://github.com/google/benchmark): use the system version if found, download and build it using the [FindGBench](https://gitlab.com/bluehood/cmake_snips/blob/master/cmake/FetchGBench.cmake) module otherwise